﻿using System;
using System.Collections.Generic;
namespace LV2
{
    class Program
    {
        static void Main(string[] args)
        {
            //----------1-------------
            /*
            DiceRoller bubanj = new DiceRoller();
            for(int i=0;i<20;i++)
            {
                Die kockica = new Die(6);
                bubanj.InsertDie(kockica);
            }
            bubanj.RollAllDice(); 
            for(int i=0 ;i < bubanj.DiceCount;i++)
            {
               Console.WriteLine( bubanj.GetRollingResults()[i]);
            }
            */
            //-------------------------
            //------------2------------
            Random rand = new Random();
            DiceRoller bubanj = new DiceRoller();
            Random gen = new Random();
            for(int i=0;i<20;i++)
            {
                Die kockica = new Die(6, gen);
                bubanj.InsertDie(kockica);
            }
            bubanj.RollAllDice(); 
            for(int i=0 ;i < bubanj.DiceCount;i++)
            {
               Console.WriteLine( bubanj.GetRollingResults()[i]);
            }
            //-------------------------
            Console.ReadLine();
        }
    }
}
